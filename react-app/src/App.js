// import logo from './logo.svg';
import { Fragment } from 'react';
import { Container } from 'react-bootstrap'
import './App.css';
//components
import AppNavbar from './components/AppNavbar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
//import CourseCard from './components/CourseCard'
//pages
import Home from './pages/Home';

function App() {
  return (
  <Fragment>
    <AppNavbar/>
    <Container>
      <Home/>
    </Container>
   
  </Fragment>
  );
}

export default App;
