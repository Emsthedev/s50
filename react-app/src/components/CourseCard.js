import { Row, Col, Card, Button } from 'react-bootstrap';

export default function Highlights() {
    return(
       <Row className="mt-3 mb-3">
        <Col xs={12} md={4}>
            <Card className="cardHighlight p-3" >
                <Card.Body >
                    <Card.Header className="text-center card-header">Sample Course</Card.Header>
                    <Card.Subtitle className="mt-4 text-">Description:</Card.Subtitle>
                    <Card.Text>
                        This is a sample course offering
                    </Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>
                        PHP 40,000
                    </Card.Text>
                    <Button variant="primary">Enroll</Button>
                   
                </Card.Body>
            </Card>
        </Col>

        
       </Row>
    )
};